

#include <WiFi.h>         
#include <PubSubClient.h>
#include <HardwareSerial.h>
#include "time.h"
#include <Preferences.h>
#include <ArduinoJson.h>
#include "MemoryLCD.h"
#include <HttpFOTA.h>
String mould[500];
String app_machine_no,app_mould_no,app_mould_name;
char dev_version[10] ="v3.1";
const char* topic_pub = "g5_dev_pub";
const char* topic_sub = "g5_demo_sub";
const char* topic_reset = "g5_dev_reset";
const char* topic_mouldPub = "g5_cloud_pub";
const char* topic_mould = "g5_dev_new_pub";
const char* topic_status = "g5_status_dev_pub";
const char* topic_response = "g5_cloud_response";
char dev_text[50];
char cloud_mouldno[50];
//WiFiManager wifiManager;

void mqttCallback(char* topic,byte* payload, unsigned int len) ;
int kj_val;
bool mould_detect = false;
bool flash_clear = false;
bool set_time = false;
bool new_found = false;
bool send_response = false;
bool master_invalid = false;
bool readytosend = true;
bool send_status = false;
int app_resp =0;
 char key[2];
String mould_data;
int Current_mld_id;
int reset_d;
int last_id ;
char recValue[50];
int new_d = 0;
int cloud_count;

void wifi_task( void * parameter );
void mqtt_task( void * parameter );
void uart_task( void * parameter );

void display1();

#define OTA_TOPIC    "mould_fota"

char buffer1[40];
void fota();
typedef enum {
  Runnning_e = 0x01,
  Fota_e  
}SysState;

WiFiClient mouldClient;
PubSubClient mqtt(mouldClient);

char url[100];
char md5check[50];
SysState state = Runnning_e;


void progress(DlState state, int percent){
  Serial.printf("state = %d - percent = %d\n", state, percent);
}


HardwareSerial MySerial(1);
char d_tag[20];

const char* ssid     = "ATP";//"In2things";//"Idea4G-SmrtWiFiHB0438";
const char* password = "admin123456789";//"in2_router@123";//"1n30q3d4";
const char* broker = "13.126.16.109";  //"35.154.15.147";
//int scanTime = 30; //In seconds

bool wifi_connect_status=false;
//bool mould_fota_flag = false;

int ledPin = 33;
const byte interruptPin = 34;
unsigned int live_Count,base_Count,bhu_Count;
uint64_t chipid;  
char id_[6];
char id_1[10];
char kid[12];
char k_id[12];

char my_machineNo[50],my_mouldName[50],my_mouldNo[50],my_liveCnt[50],my_mainCnt[50],my_baseCnt[50];
int live1_count,main_count;

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 9600;
const int   daylightOffset_sec = 9600;
time_t now;
Preferences preferences;

//#if defined (ESP32)
//const int SW3 = 35;
//const int SW2 = 34;
//const int BUZZ = 27;
//#elif defined (_VARIANT_ARDUINO_ZERO_)
//const int SW3 = 6;
//const int SW2 = 5;
//const int BUZZ = 7;
//#endif

extern const BFC_FONT fontConsolas24h;
extern const BFC_FONT fontConsolas32h;
extern const BFC_FONT fontArialRoundedMTBold28h;
extern const BFC_FONT fontArialRoundedMTBold33h;
extern const BFC_FONT fontArialUnicodeMS21h;
extern const tImage atplogo;

int s1_x=0,s1_y=0;

bool qrcode_app=false;

unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 100;    // the debounce time; increase if the output flickers

//void buzz(int pin, unsigned int freq, unsigned long duration);
//void buzz(int pin, unsigned int freq, unsigned long duration)
//{
//#if defined (ESP32)
//  ledcWriteTone(0, (double)freq);
//  ledcAttachPin(BUZZ, 0);
//  delay(duration);
//  ledcDetachPin(BUZZ);
//#else
//  tone(pin,freq,duration);
//#endif
//}

int32_t pm_count,life_count;

SemaphoreHandle_t xSemaphore = NULL;


portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

void error(char *message){
  printf("%s\n", message);
}

void startDl(void)
{

}
void endDl(void)
{

}

void IRAM_ATTR handleInterrupt() {
  portENTER_CRITICAL_ISR(&mux);
  live_Count++;
    bhu_Count++;
//  lastDebounceTime = millis();
//  if ((millis() - lastDebounceTime) > debounceDelay)
//  {
//    live_Count++;
//    bhu_Count++;
//  }
  portEXIT_CRITICAL_ISR(&mux);
}

void reconnect() {
  // Loop until we're reconnected
  if (!mqtt.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqtt.connect("TestClientID")) {
      Serial.println("connected");
      // Subscribe
    mqtt.setCallback(mqttCallback);
 // vTaskDelay(2000);
 //      delay(2000);
     mqtt.subscribe(topic_sub);
     delay(1000);
     mqtt.subscribe(topic_reset);
     delay(1000);
     mqtt.subscribe(topic_mouldPub);
      delay(1000);
     mqtt.subscribe(OTA_TOPIC);
      delay(1000);
   //   mqtt.subscribe(topic_status);
       delay(1000);
     mqtt.subscribe(topic_response);
    delay(1000);
      


    } else {
      Serial.print("failed, rc=");
     // Serial.print(mqttclient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void printLocalTime()
{
  delay(100);
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
      configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
      set_time=false;

    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  set_time=true;
  
}

void mqttCallback(char* topic, byte* payload, unsigned int len) 
{
  Serial.println("callback event occurs");
  digitalWrite(ledPin, HIGH);
  delay(500);
  digitalWrite(ledPin, LOW);
  Serial.write(payload, len);

  StaticJsonBuffer<256> jsonBuffer;   
  JsonObject& root = jsonBuffer.parseObject(payload);

  
  if(strcmp(topic , topic_sub )==0)  
  {
      pm_count = root["pm_count"];
      life_count = root["life_count"];
        reset_d = root["reset"];
       Serial.println(reset_d);

        if(reset_d == 0)
//        {reset_base = true;}

     // check_count = true;
     Serial.println(pm_count);
     Serial.println(life_count);
     send_response = true;
  }
  if(strcmp(topic , topic_reset )==0)  
  {
    
    kj_val = root["reset"];
    if(kj_val == 1)
    {
      flash_clear = true;
      }
    send_response = true;

  }

    if(strcmp(topic , topic_status )==0)  
  {
    
    int  version1 = root["version"];
    if(version1 == 1)
    {
      send_status = true;
      }

  }

    if(strcmp(topic , topic_mouldPub )==0)  
  {
   // app_resp = false;
    const char*  source = root["source"];
    String  machine_no = root["machinename"];
    String  mould_no = root["mouldnumber"];
    String  mould_name = root["mouldname"];
    live1_count = root["tool_shots"];
    main_count = root["maint_shots"];
    cloud_count = (int)root["life_count"];
    if(mould_no == d_tag  && (strcmp("dev",source)==0)){
      Serial.println("in dev");
      master_invalid = false;
      base_Count = cloud_count;
      base_Count += bhu_Count;
      mould_detect = true;
//      d_tag = mould_no;
     // sprintf(d_tag, "%s", mould_no);
       // sprintf(my_machineNo,"%s",machine_no.c_str());
       // sprintf(my_mouldName,"%s",mould_name.c_str());
      mould[Current_mld_id+4]=mould_no;
      mould[Current_mld_id+5]=mould_name;
      mould[Current_mld_id+1]=live1_count;
      mould[Current_mld_id+2]=main_count;
      mould[Current_mld_id+6]=machine_no;
      sprintf(key, "%d",Current_mld_id+4);
      preferences.putString(key, (String)mould_no);
      sprintf(key, "%d",Current_mld_id+5);
      preferences.putString(key, (String)mould_name); 
      sprintf(key, "%d",Current_mld_id+1);
      preferences.putString(key, (String)live1_count);
      sprintf(key, "%d",Current_mld_id+2);
      preferences.putString(key, (String)main_count); 
      sprintf(key, "%d",Current_mld_id+6);
      preferences.putString(key, (String)machine_no);
      qrcode_app=true;
      readytosend =true;
    }
    else
    {
      Serial.printf("\r\n in callback ELSE %s", source);      
      if(strcmp("app",source)==0)
      {
         master_invalid = false;
         Serial.println("APP matched");
        app_machine_no = machine_no;
        app_mould_no = mould_no;
        app_mould_name= mould_name;
        app_resp = 1;
       /* if(xSemaphoreTake( xSemaphore, app_resp ) == pdTRUE)
        {
          app_resp = true;
          xSemaphoreGive( app_resp );
        }*/
        sprintf(cloud_mouldno,"%s",mould_no.c_str());
        readytosend = false;
        }
      
      }
  }

    if (strncmp(OTA_TOPIC, topic, strlen(OTA_TOPIC)) == 0) {
    memset(url, 0, 100);
    memset(md5check, 0, 50);
    char *tmp = strstr((char *)payload, "url:");
    char *tmp1 = strstr((char *)payload, ",");
    memcpy(url, tmp+strlen("url:"), tmp1-(tmp+strlen("url:")));
    
    char *tmp2 = strstr((char *)payload, "md5:");
    memcpy(md5check, tmp2+strlen("md5:"), len-(tmp2+strlen("md5:")-(char *)&payload[0]));

    Serial.printf("started fota url: %s\n", url);
    Serial.printf("started fota md5check: %s\n", md5check);
    state = Fota_e;
   fota();
  }
    if (strncmp(topic_response, topic, strlen(topic_response)) == 0){
  String gateway_mac  = root["dev_id"];
  String mouldId = root["tag_id"];
  String  dev_message = root["message"];
    Serial.println("RESPONSE RECIVED");

    if(gateway_mac == k_id  && mouldId == d_tag ){
      master_invalid = true;
      sprintf(dev_text,"%s",dev_message.c_str());
  Serial.println("master invalid");
  
    }
  
  }
 
}

void wifi_task( void * parameter )
{
    Serial.println("WiFi init starting");
    WiFi.begin(ssid, password);

    int timeout= millis();

    while (WiFi.status() != WL_CONNECTED && millis() - timeout < 20000L);
    if (WiFi.status() == WL_CONNECTED)
    {
      Serial.println("wifi connected");
      digitalWrite(ledPin, HIGH);
      vTaskDelay(2000);
      digitalWrite(ledPin, LOW);
     

    }
//    while(WiFi.status() != WL_CONNECTED) {
//        vTaskDelay(500);
//        //Serial.print(".");
//    }    

    //Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    wifi_connect_status=true;
xTaskCreate( mqtt_task,"mqtt_task",5000,NULL, 3,   NULL);

    while(1)
    {
      if(WiFi.status() != WL_CONNECTED){
  WiFi.begin(ssid,password);
  //WiFi.reconnect();
    vTaskDelay(2000);
    if (WiFi.status() != WL_CONNECTED) {
    vTaskDelay(5000);
    //Serial.println("Connecting to WiFi..");
  }
  Serial.print("WiFi connected, IP address: "); 
  Serial.println(WiFi.localIP());

 }
      vTaskDelay(2000); 
    }
}
 
void mqtt_task( void * parameter)
{ 

  vTaskDelay(10000);
  Serial.println("mqtt init starting");
  while(!wifi_connect_status); 
  mqtt.setServer(broker, 1883);
  //mqtt.setCallback(mqttCallback);
  
  Serial.print("Connecting to");
  Serial.print(broker);
  boolean status = mqtt.connect("TestClientID");
  if(status == false) 
  {    
    Serial.println("MQTT connection fail");
    vTaskDelay(1000);
    //ESP.restart();
  }
  else
  {
    char dev_info[30];
    Serial.println("MQTT connected");
     mqtt.setCallback(mqttCallback);
    digitalWrite(ledPin, HIGH);
    vTaskDelay(2000);
    digitalWrite(ledPin, LOW);

     sprintf(dev_info,"{\"dev_id\":\"%s\",\"version\":\"%s\"}",k_id,dev_version); 
      mqtt.publish(topic_status,dev_info);

  }
 //  mqtt.setCallback(mqttCallback);
 // vTaskDelay(2000);
 //      delay(2000);
     mqtt.subscribe(topic_sub);
     vTaskDelay(1000);
      mqtt.subscribe(topic_response);
     vTaskDelay(1000);
     mqtt.subscribe(topic_mouldPub);
      vTaskDelay(1000);
     mqtt.subscribe(OTA_TOPIC);
      vTaskDelay(1000);
   //  mqtt.subscribe(topic_status);
      vTaskDelay(1000);
     mqtt.subscribe(topic_reset);
    vTaskDelay(1000);
       //mqtt.publish("aqua_status", "This is a test msg");
  while(1)
  {

   if (!mqtt.connected()) {
    vTaskDelay(5000);
    reconnect();
  }
  if(set_time == false)
  {
    printLocalTime();
    }
  if(flash_clear){
   preferences.clear();
   vTaskDelay(1000);
  preferences.putUInt("last_index",7);

   flash_clear = false;
   //ESP.restart();
  }
  if(send_status)
  {
    
    send_status = false;
  //  mqtt.publish("dev_status","2");
    }
  char dev_data[500];
  base_Count+=live_Count;
  Serial.println("reading index and base value");
  Serial.println(Current_mld_id);
  Serial.println(base_Count);
  Serial.println("exit");
  sprintf(key, "%d", Current_mld_id + 3);
  preferences.putString(key, (String)base_Count);
  if(strlen(d_tag)>0){
    Serial.println("by name");
    sprintf(dev_data, "{ \"dev_id\":\"%s\",\"time\":\"%d\",\"tag_id\":\"%s\",\"liv_cnt\":\"%d\",\"base_cnt\":\"%d\" }",k_id,time(&now),d_tag,bhu_Count ,base_Count);
  }
  else{
        Serial.println("by id");

    sprintf(dev_data, "{ \"dev_id\":\"%s\",\"time\":\"%d\",\"tag_id\":\"%s\",\"liv_cnt\":\"%d\",\"base_cnt\":\"%d\" }",k_id,time(&now),mould[Current_mld_id].c_str(),bhu_Count ,base_Count);
  }
  if(mould_detect && mqtt.connected())  { 

       digitalWrite(ledPin, HIGH);
       mqtt.publish(topic_pub,dev_data);
       vTaskDelay(500);
       digitalWrite(ledPin, LOW);
       
       mould_detect = false;
             bhu_Count=0;

       }
    live_Count=0;
     Serial.println(dev_data);
    mqtt.loop(); 
    if(qrcode_app) 
    {
      GFXDisplayAllClear();
      qrcode_app=false;
    }
    if(mqtt.connected()){
    digitalWrite(ledPin, HIGH);
       

       mqtt.publish(topic_pub,dev_data);
       bhu_Count=0;
       vTaskDelay(1000);
       digitalWrite(ledPin, LOW);
    }
     display1(); //Sharad
    vTaskDelay(10000); 
    mqtt.loop(); 
  }
}

void uart_task( void * parameter )
{

  int i = 0;
  boolean data_rec = false;
  int data_app = 0;
 // app_resp=false;

  while (1)
  {
    int flag_local = 0;
/*
    while (MySerial.available() > 0)
    {
      char byteFromSerial = MySerial.read();
      recValue[i++] = byteFromSerial;
      data_rec = true;

      // Do something
    }*/
   /* if(app_resp == 1)   
    { 
      Serial.printf("cloud_mouldno coming from APP is ");     
      for(int j=0;j<sizeof(cloud_mouldno);j++)
          Serial.printf("%c",cloud_mouldno[j]);
    }*/

   
    if ((data_rec &&  recValue[0]=='T' && recValue[1]=='L') ||(app_resp==1 && cloud_mouldno[0]=='T' && cloud_mouldno[1]=='L' ) ) //(strnccmp(recValue, "TL",2)) // &&  recValue[0]=='T' && recValue[1]=='L'
    {
      //Serial.printf("\r\nin matched...........................%s\r\n", cloud_mouldno);
      
      if(app_resp ==0){
        memset(d_tag, '\0', sizeof(d_tag));
      strcpy(d_tag, recValue);
      }
      if(app_resp == 1)
      {
       app_resp = 0;
       data_app = 1;
        //d_tag =app_mould_no;//cloud_mouldno;
        memset(d_tag, '\0', sizeof(d_tag));
        sprintf(d_tag, "%s",app_mould_no.c_str());
       // for(int i=0;i<sizeof(cloud_mouldno);i++) 
        //{ 
         // d_tag[i]=cloud_mouldno[i]; 
        //} 
        //strcpy(d_tag.c_str(), cloud_mouldno);
      /*  for(int j=0;j<5;j++)
          Serial.printf("cloud_mouldno coming from APP is :%c",d_tag[j]);
         Serial.printf("\n\r");
        memset(cloud_mouldno,0,sizeof(cloud_mouldno));
        }*/
      //char new_dev1[200];
      //sprintf(new_dev1,"{\"dev_id\":\"%s\",\"mould_id\":\"%s\"}",k_id,d_tag.c_str()); 
      //mqtt.publish(topic_mould,new_dev1);

      for (int i = 0; i < 500; i = i + 7  )
      {
        if (mould[0].length() == 0) 
        {
                    Serial.println("break");

          break;
        } else 
        {
          //char scan_mould[20];
          //strcpy(scan_mould, d_tag.c_str()); 
         // Serial.printf("\r\nmould[i]+++++++++++++ %s\n",mould[i].c_str());
         if ( mould[i]== d_tag)
        {
          Serial.println("match found");
          Serial.printf("index is -- %d\n",i);
          Serial.printf("Curent index is -- %d\n",Current_mld_id);
          mould_detect = true;
          flag_local = 1;
         // base_Count = mould[i + 3].toInt();
          //if(Current_mld_id!=i)
          if(true)
          {
            Current_mld_id = i;
            live_Count=0;
            sprintf(key, "%d",i+3);
            base_Count = (preferences.getString(key,"0")).toInt();
            if(data_app==0)
            {
            char new_dev1[200];
            sprintf(new_dev1,"{\"dev_id\":\"%s\",\"tag_id\":\"%s\",\"base_cnt\":\"%d\"}",k_id,d_tag,base_Count); 
            mqtt.publish(topic_mould,new_dev1);
            }
            if(data_app==1)
            {
              Serial.println("match found base cnt");
               base_Count = cloud_count;
               base_Count += bhu_Count;
                mould_detect = true;
              //  d_tag = app_mould_no;
                 // sprintf(my_machineNo,"%s",machine_no.c_str());
                 // sprintf(my_mouldName,"%s",mould_name.c_str());
                mould[Current_mld_id+4]=app_mould_no;
                mould[Current_mld_id+5]=app_mould_name;
                mould[Current_mld_id+1]=live1_count;
                mould[Current_mld_id+2]=main_count;
                mould[Current_mld_id+6]=app_machine_no;
                sprintf(key, "%d",Current_mld_id+4);
                preferences.putString(key, (String)app_mould_no);
                sprintf(key, "%d",Current_mld_id+5);
                preferences.putString(key, (String)app_mould_name); 
                sprintf(key, "%d",Current_mld_id+1);
                preferences.putString(key, (String)live1_count);
                sprintf(key, "%d",Current_mld_id+2);
                preferences.putString(key, (String)main_count); 
                sprintf(key, "%d",Current_mld_id+6);
                preferences.putString(key, (String)app_machine_no);
                qrcode_app=true;
                //app_resp = false;
                readytosend = true;
                        
              }

             GFXDisplayAllClear();
          }
          
          Serial.println(Current_mld_id);
          preferences.putUInt("last_scanid",Current_mld_id);

          //preferences.putUInt("current_index", Current_mld_id);
          break;
        }
        }
      }
      if (flag_local == 0)
      {
       // new_found = true;
        Serial.println("new found");
        int last_current_mld_id = preferences.getUInt("last_index", 0);
        Current_mld_id = last_current_mld_id;
        Serial.println(last_current_mld_id);
       
        if(data_app==0)
        {
        mould[last_current_mld_id] = d_tag;
        preferences.putUInt("last_index", last_current_mld_id + 7);
        sprintf(key, "%d", last_current_mld_id);
        preferences.putString(key, d_tag);
        Serial.println("dev stored");
          
        Current_mld_id=last_current_mld_id;
        memset(key, 0, sizeof(key));
        live_Count=0;
        bhu_Count=0;
        preferences.putUInt("last_scanid",Current_mld_id);
        base_Count = 0;
        char new_dev1[200];
        sprintf(new_dev1,"{\"dev_id\":\"%s\",\"tag_id\":\"%s\",\"base_cnt\":\"%d\"}",k_id,d_tag,base_Count); 
        mqtt.publish(topic_mould,new_dev1);
        readytosend = false;
        GFXDisplayAllClear();
        }
        if(data_app==1)
        {
          data_app = 0;
          mould[last_current_mld_id] = d_tag;
          preferences.putUInt("last_index", last_current_mld_id + 7);
         sprintf(key, "%d", last_current_mld_id);
          preferences.putString(key, (String)d_tag);
          Serial.println("app stored");
          Current_mld_id=last_current_mld_id;
          memset(key, 0, sizeof(key));
          live_Count=0;
          bhu_Count=0;
          preferences.putUInt("last_scanid",Current_mld_id);
          base_Count = 0;
 
               base_Count = cloud_count;
               base_Count += bhu_Count;
                mould_detect = true;
                //d_tag = app_mould_no;
                 // sprintf(my_machineNo,"%s",machine_no.c_str());
                 // sprintf(my_mouldName,"%s",mould_name.c_str());
                mould[Current_mld_id+4]=app_mould_no;
                mould[Current_mld_id+5]=app_mould_name;
                mould[Current_mld_id+1]=live1_count;
                mould[Current_mld_id+2]=main_count;
                mould[Current_mld_id+6]=app_machine_no;
                sprintf(key, "%d",Current_mld_id+4);
                preferences.putString(key, (String)app_mould_no);
                sprintf(key, "%d",Current_mld_id+5);
                preferences.putString(key, (String)app_mould_name); 
                sprintf(key, "%d",Current_mld_id+1);
                preferences.putString(key, (String)live1_count);
                sprintf(key, "%d",Current_mld_id+2);
                preferences.putString(key, (String)main_count); 
                sprintf(key, "%d",Current_mld_id+6);
                preferences.putString(key, (String)app_machine_no);
                qrcode_app=true;
                readytosend = true;
          
          
          }

      }
     
      i = 0;
      data_rec = false;
      memset(recValue, 0, sizeof(recValue));
     
      //new_found = false;
      
    }
   /* if(new_found){
    //  char new_dev[200];
   // sprintf(new_dev,"{\"gateway_Id\":\"%s\",\"mould_Id\":\"%s\"}",k_id,d_tag.c_str()); 
   // mqtt.publish(topic_mould,new_dev);
   // Serial.println(new_dev);
  //  memset(new_dev, 0, sizeof(new_dev));
    
    }*/
    
   

    
  }

   vTaskDelay(10000);

}

}

void setup() 
{ 
  Serial.begin(115200);
  MySerial.begin(9600, SERIAL_8N1, 16, 17);
  preferences.begin("wifi", false);
  Current_mld_id=0;

  pinMode(ledPin, OUTPUT);
pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, RISING);

  hal_bsp_init();
  GFXDisplayPowerOn();
//  pinMode(SW3, INPUT_PULLUP); //set SW3 pin as input
//  pinMode(SW2, INPUT_PULLUP); //set SW2 pin as input
#if defined (ESP32)
///@note  Ref: https://github.com/espressif/arduino-esp32/blob/a4305284d085caeddd1190d141710fb6f1c6cbe1/cores/esp32/esp32-hal-ledc.h#L30
ledcSetup(0, 1000, 8);  //channel 0, freq=1000, resolution_bits = 8
#endif
  
chipid=ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).
 sprintf(id_,"%04X",(uint16_t)(chipid>>32));
sprintf(id_1,"%08X\n",(uint32_t)chipid);
strcat(kid,id_);
strcat(kid,id_1);
sprintf(k_id,"%c%c:%c%c:%c%c:%c%c:%c%c:%c%c",kid[10],kid[11],kid[8],kid[9],kid[6],kid[7],kid[4],kid[5],kid[2],kid[3],kid[0],kid[1]);

 mould[0] = "dummy";
 mould[1] = "0";
 mould[2] = "0";
 mould[3] = "0";
 mould[4] = "0";
 mould[5] = "dummy_mould";
 mould[6] = "dummy_machine";


 for(int i=0; i<7;i++)
 {
    sprintf(key, "%d", i);
    preferences.putString(key,mould[i]);
    memset(key, 0, sizeof(key));
 }
 
 Current_mld_id=preferences.getUInt("last_scanid",0);
 base_Count=mould[Current_mld_id+3].toInt();
  delay(1000);
 for (int i = 0; i < 500; i++)
  {
    sprintf(key, "%d", i);
    mould[i] = preferences.getString(key, "0");
   // Serial.println(mould[i]);
    memset(key, 0, sizeof(key));
  }
  base_Count=mould[Current_mld_id+3].toInt();
   Serial.printf("\n\r base_cnt=%d",base_Count);

 // app_resp=false;
  //mould_data.toCharArray(mould,10);

   // wifiManager.autoConnect("IN2THINGS_DEV","i2t@123456");
     display1();
delay(2000);
 
  xTaskCreate(wifi_task, "wifi_task", 5000, NULL, 2, NULL);            /* wifi task */
  //xTaskCreate(mqtt_task, "mqtt_task", 5000, NULL, 1, NULL);
  xTaskCreate(uart_task, "uart_task", 5000, NULL, 1, NULL);            /* wifi task */

//configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  //printLocalTime();
//display1();

}
 
void display1()
{

       Serial.println("in display func");
              GFXDisplayPutString(s1_x+20,s1_y+10,&fontConsolas32h, "MOULD MONITORING", BLACK, WHITE);
    //  GFXDisplayPutString(s1_x+115,s1_y+10,&fontConsolas32h, "MONITORING v2", BLACK, WHITE);

       GFXDisplayPutImage(s1_x+330,s1_y+10,&atplogo,0);

       GFXDisplayPutString(s1_x+340,s1_y+215,&fontArialUnicodeMS21h, dev_version, BLACK, WHITE);

       if(master_invalid)
       {
        GFXDisplayPutString(s1_x+20,s1_y+65,&fontConsolas32h, "                  ", BLACK, WHITE);
        GFXDisplayPutString(s1_x+300,s1_y+65,&fontConsolas32h, "              ", BLACK, WHITE);
        GFXDisplayPutString(s1_x+190,s1_y+100,&fontConsolas32h, "                 ", BLACK, WHITE);

        GFXDisplayPutString(s1_x+20,s1_y+65,&fontConsolas32h, dev_text, BLACK, WHITE);
        }
        else{
          //  GFXDisplayPutString(s1_x+20,s1_y+65,&fontConsolas32h, "                                                   ", BLACK, WHITE);
       GFXDisplayPutString(s1_x+20,s1_y+65,&fontConsolas24h,mould[Current_mld_id+6].c_str(), BLACK, WHITE);
       GFXDisplayPutString(s1_x+300,s1_y+65,&fontArialRoundedMTBold28h,mould[Current_mld_id+4].c_str(), BLACK, WHITE);
       GFXDisplayPutString(s1_x+190,s1_y+100,&fontConsolas24h,mould[Current_mld_id+5].c_str(), BLACK, WHITE);    

        }

//
//       GFXDisplayPutString(s1_x+20,s1_y+65,&fontConsolas24h,mould[Current_mld_id+6].c_str(), BLACK, WHITE);
//        
//       //sprintf(my_mouldNo,"%d",mould_no);
//       GFXDisplayPutString(s1_x+300,s1_y+65,&fontArialRoundedMTBold28h,mould[Current_mld_id+4].c_str(), BLACK, WHITE);
//       GFXDisplayPutString(s1_x+190,s1_y+100,&fontConsolas24h,mould[Current_mld_id+5].c_str(), BLACK, WHITE);    
////      GFXDisplayPutImage(360,5,&atplogo,0);///

        memset(my_baseCnt, '\0', sizeof(my_baseCnt));
        sprintf(my_baseCnt,"%d",base_Count);

         GFXDisplayPutString(s1_x+20,s1_y+135,&fontArialRoundedMTBold33h, "              ", BLACK, WHITE);
        GFXDisplayPutString(s1_x+20,s1_y+135,&fontArialRoundedMTBold33h, my_baseCnt, BLACK, WHITE);
        GFXDisplayPutString(s1_x+20,s1_y+175,&fontConsolas24h, "Real", BLACK, WHITE);
        GFXDisplayPutString(s1_x+20,s1_y+195,&fontConsolas24h, "shots", BLACK, WHITE);
        //memset(my_baseCnt, '\0', sizeof(my_baseCnt));
        
       // sprintf(my_mainCnt,"%d",main_count);
       
       // GFXDisplayPutString(s1_x+130,s1_y+135,&fontArialRoundedMTBold33h, "                ", BLACK, WHITE);
        GFXDisplayPutString(s1_x+130,s1_y+135,&fontArialRoundedMTBold33h, mould[Current_mld_id+2].c_str(), BLACK, WHITE);
        GFXDisplayPutString(s1_x+130,s1_y+175,&fontConsolas24h, "Prev.maint", BLACK, WHITE);
        GFXDisplayPutString(s1_x+130,s1_y+195,&fontConsolas24h, "shots", BLACK, WHITE);

        // sprintf(my_liveCnt,"%d",live1_count);
     //   GFXDisplayPutString(s1_x+270,s1_y+135,&fontArialRoundedMTBold33h,"                   ", BLACK, WHITE);
        GFXDisplayPutString(s1_x+270,s1_y+135,&fontArialRoundedMTBold33h,mould[Current_mld_id+1].c_str(), BLACK, WHITE);
        GFXDisplayPutString(s1_x+270,s1_y+175,&fontConsolas24h, "ToolLife", BLACK, WHITE);
        GFXDisplayPutString(s1_x+270,s1_y+195,&fontConsolas24h, "shots", BLACK, WHITE);
        
}

void fota(void)
{
   switch (state)
  {
    case Runnning_e:
      /* if client was disconnected then try to reconnect again */
      if (!mqtt.connected()) {
        mqtt.connect("TestClientID");
      }
      /* this function will listen for incomming
        subscribed topic-process-invoke receivedCallback */
      mqtt.loop();
      break;
    case Fota_e:
    
      detachInterrupt(digitalPinToInterrupt(interruptPin)) ;
      
      DlInfo info;
      info.url = url;
     
      info.caCert = NULL;//if only use http then remember to set this to NULL
     // info.md5 = md5check;
      info.startDownloadCallback =  startDl;
      info.endDownloadCallback =    endDl;
      info.progressCallback  = progress;
      info.errorCallback     = error;
       Serial.println("fota");
      mqtt.publish(OTA_TOPIC, "fota_started");
      digitalWrite(ledPin, HIGH);
      httpFOTA.start(info);

      mqtt.publish(OTA_TOPIC, "ok");
      break;
    default:
      break;

  }
}

void loop() 
{
  //delay(1000);
} 
